#!/bin/bash

if [ "$1" != "--no-color" ]; then
	RED='\033[0;31m'
	GREEN='\033[0;32m'
	RESET='\033[0m'
else
	RED=
	GREEN=
	RESET=
fi

for f in database/*.in;
do
	output=$(diff "${f/in/out}" <(../src/runchess < "$f"))
	testname=$(head -1 $f | cut -d \" -f 2)
	if [ "$output" = "" ]
	then
		echo -e "${GREEN}Test: \"$testname\" passed${RESET}"
	else
		echo -e "${RED}Test: \"$testname\" failed${RESET}"
		diff --suppress-common-lines -y -W 50 "${f/in/out}" <(../src/runchess < "$f")
	fi
	echo "##################################################"
done
