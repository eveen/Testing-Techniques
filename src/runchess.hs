-- vim: sw=2 ts=2 et:
import Control.Monad
import Control.Monad.Trans.State.Lazy

import Data.Attoparsec.ByteString.Char8
import Data.ByteString.Char8 (pack)
import Data.Maybe

import Chess
import Chess.FEN
import Chess.PGN

main = readPGN >>= mapM_ applyPGN

readPGN :: IO [PGN]
readPGN = do
  pgn <- pack <$> getContents
  case parseOnly pgnParser pgn of
    Left  err   -> error err
    Right []    -> error "no games"
    Right games -> return games

applyPGN :: PGN -> IO ()
applyPGN pgn = printPGN pgn >> mapM_ printBoard (seqList [moveSAN m | m <- moves pgn] $ Right defaultBoard)
  where
    seqList (f:fs) (Right io) = f io:seqList fs (f io)
    seqList _      (Left _)   = []
    seqList []     _          = []

printBoard (Left  e) = putStrLn $ show e
printBoard (Right b) = putStrLn $ show b

printPGN :: PGN -> IO ()
printPGN pgn = do
  putStrLn $ site pgn ++ " " ++ date pgn ++ ": " ++ whitePlayer pgn ++ " vs. " ++ blackPlayer pgn
  putStrLn $ show $ result pgn
