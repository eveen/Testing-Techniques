\documentclass[british]{scrartcl}

\usepackage[british]{babel}
\usepackage{csquotes}
\usepackage{enumerate}
\usepackage[hidelinks]{hyperref}
\usepackage{minted}
\setminted{fontsize=\small,breaklines,tabsize=4}
\newmintinline[hs]{haskell}{style=bw}
\newmintinline[console]{console}{style=bw}
\usepackage{caption}
\newenvironment{longlisting}{\captionsetup{type=listing}}{}
\usepackage{tikz}
\usetikzlibrary{arrows, matrix, positioning}
\usepackage{skak}
\usepackage{cleveref}

\title{Model-Based Testing with QuickCheck}
\subtitle{Testing \texttt{chesshs} with QuickCheck}
\author{Ren\'e den Hertog \and Camil Staps \and Erin van der Veen}

\begin{document}

\maketitle

\section{Introduction}
In this report we discuss how we tested the Haskell library \texttt{chesshs}%
	\footnote{\url{https://hackage.haskell.org/package/chesshs}}
	using QuickCheck.
QuickCheck\footnote{\url{http://www.cse.chalmers.se/~rjmh/QuickCheck/}} is a property based testing tool initially created for Haskell.
Given that our SUT is a Haskell library and that QuickCheck is recommended in the assignment, using QuickCheck is nothing but a logical consequence.

The results described here can be reproduced with the attached code.
With \console{make run}, the test program can be compiled and run.
When there is no dataset for generating test cases present, \console{make run} will download one.

\section{Modelling}
We decide to test the move verification functionality and not the parser.
The reason for this is that the parser is less well-defined, as we saw in our previous assignment.
Also, it seems relatively easy to break the parser.
We would prefer to fix the obvious problems before applying automated testing,
	but that is outside the scope of the current assignment.
Lastly, the move verification functionality is more interesting because of the larger search space and complexity.

As mentioned above, QuickCheck is a property-based testing tool.
A property based testing tool can test if certain specifications (lists of properties) of a program/function hold.
This is done through generating a large number of test cases.

In QuickCheck, test cases are generated with the \hs{Arbitrary} class.
This class implements has two members, \hs{arbitrary :: Gen a} and \hs{shrink :: a -> [a]}.
The first generates random, usually large, test cases.
When a counter-example is found, QuickCheck uses \hs{shrink} to find a smaller counter-example by shrinking the known counter-example while preserving the inconsistency.
The implementation of \hs{shrink} is usually straightforward for recursive data types,
	but more complex in the case of Chess.
To write a good \hs{shrink} for a list of Chess moves,
	one would need to consider characteristics of the board
	and construct a shorter list of moves that lead to a board with similar characteristics.
However, it is unclear what these characteristics should be, as this highly depends on the test.
For this reason, we have decided to not implement \hs{shrink} and use an analysis tool to inspect the found counter-examples.%
	\footnote{For example \url{https://www.chess.com/analysis-board-editor}.}

The above implies that a model of the system is given using a specification.
In our case, the model is a collection of predicates which should hold.
We compile a test program with \texttt{chesshs} and QuickCheck and run it.
So, unlike in assignment 1, we do not use a wrapper around the library, but interact directly with it.
Every property that is tested can be found in the \texttt{Test.hs} file and has the prefix \enquote{prop}.

\section{Test Environment}
Since our SUT is a library, we did not have to make any changes to it.
We did add some helper/wrapper functions that allow us to communicate with the SUT in a more convenient manner.
An example of such a function is \hs{pieceCoords :: Maybe Color -> Maybe PieceType -> Board -> [((Int,Int),Piece)]}, which allows easy inspection of a board.

Like our SUT, QuickCheck is technically a Haskell library.
This means that, given our properties, QuickCheck can directly communicate with the \texttt{chesshs} library.
In turn, this implies (as mentioned above) that the wrapper from assignment 1 is not used.
More generally, no wrapper or adapter of any kind is used whatsoever.

As explained above, we need to implement the \hs{Arbitrary} class for the types used in \texttt{chesshs} to automatically generate test cases.
We focus here on the generation of moves.
A straightforward way to do this is to assign probabilities to different aspects of each move,
	e.g. the piece that is moved, whether the move causes check(mate), whether a piece is taken, etc.
Then we can generate random moves based on those probabilities.
However, it turns out that when we generate games (i.e., lists of moves) with this approach,
	we generate only very few legal games:
	roughly 1\% of all generate games contain at least one legal move.
Hence, with this approach we miss most interesting situations, like check, taking \emph{en passant}, castling, etc.

Another approach would be to use a database of Chess games and use those as test cases.
This approach has the disadvantage that we would only test on legal moves:
	we would not test whether the \texttt{chesshs} allows a king to be taken, castling to be performed twice, black moving a white piece, etc.

We take yet a different approach.
We train a Markov Chain model based on games from a database, taken from The Week in Chess.%
	\footnote{%
		\url{http://theweekinchess.com/}.
		The main reason for taking (a subset of) this database is that it is easy to crawl.
		The test setup is agnostic with regards to the source of the data.}
We use the model to generate new random games.
This approach generates many games with many legal moves (about 70\% has more than 10 legal moves).
It also generates games with moves that were not included in the random tests,
	such as castling (35\%) and check (6\%).%
	\footnote{%
		It is difficult to test whether a game includes taking \emph{en passant},
			since our database does not use the notation \enquote{e.p.} for marking these moves.
		We have decided to focus on implementing and analysing our tests more thoroughly rather than checking whether the generated games contain such moves.}
Furthermore, it generates illegal moves, as we will see below.

A Markov Chain model depends on one parameter, the size of the prediction context
	(i.e., the number of moves the generator should look back to predict the next move).
We set this parameter to 4,
	because setting it to 3 leads to a large number of small test cases (over 60\% with less than 10 legal moves)
	and setting it to 5 gave similar results as setting it to 4.


\section{Testing}
We decide to test on a number of basic invariants that hold for Chess in general,
	rather than implementing all the details of the game ourselves
	(which would only move the test obligation from the \texttt{chesshs} library to our tests).
The complete list of tests is found in \cref{tab:tests}.

\begin{table}[h]
	\centering
	\begin{tabular}{l p{\linewidth-6cm}}
		Name                            & Description \\\hline
		\hs{prop_checkPGN}              & The string representation of a PGN can be parsed as that PGN. \\
		\hs{prop_only_2_kings}          & At any point, a game should have exactly one White and exactly one Black king. \\
		\hs{prop_no_pawns_on_1_and_8}   & Pawns should not appear at rank 1 or 8: promotion must happen immediately. \\
		\hs{prop_number_of_pieces}      & After a move, the number of pieces should be equal to or one less than the previous number of pieces. \\
		\hs{prop_not_in_check_twice}    & A player cannot be in check in two consecutive turns. \\
		\hs{prop_move_not_result_check} & A player cannot cause himself to be in check. \\
		\hs{prop_dont_touch_my_pieces}  & A player cannot move the pieces of the opponent. \\
		\hs{prop_move_legality}         & Pieces move according to the rules:
			this takes basic rules into account, like the eight fields a knight can move to.
			More complex and derivative rules, like that it should be impossible to cause yourself to be check,
				are handled by other tests.
			It is only implemented for the king (including castling) as a proof of concept. \\
		\hs{prop_capture_must_capture}  & The number of pieces on the board should decrement iff a move contains \texttt{x}. \\
		\hs{prop_nr_of_moving_pieces}   & Only one piece should move per ply (or two in case of castling). \\
	\end{tabular}
	\caption{Tests\label{tab:tests}}
\end{table}

All but one test pass on 10,000 randomly generated games.
We found a bug with \hs{prop_capture_must_capture},
	where \texttt{chesshs} allows a piece to be taken while the move does not include the \texttt{x} character.
In the situation of \cref{fig:counter-capture-must-capture}, a second d4 is interpreted as cxd4,
	while the PGN specification says~\cite[8.2.3.3]{PGN}:

\begin{quote}
	Capture moves are denoted by the lower case letter \enquote{x} immediately prior to the destination square;
	pawn captures include the file letter of the originating square of the capturing pawn immediately prior to the \enquote{x} character.
\end{quote}

Hence, d4 should not be allowed by the library in this situation.
We have filed a bug report for this.%
	\footnote{\url{https://github.com/ArnoVanLumig/chesshs/issues/3}}

\begin{figure}[h]
	\newgame
	\fenboard{rnbqkb1r/pp1ppppp/8/3nP3/3p4/2P5/PP3PPP/RNBQKBNR w KQkq - 1 5}
	$$\showboard$$
	\caption{Board after 1. e4 c5 2. d4 cxd4 3. c3 Nf6 4. e5 Nd5.\label{fig:counter-capture-must-capture}}
\end{figure}

The test generated by QuickCheck inspire less confidence than the manual tests we created for the first assignment.
This is mostly due to the fact that we could create specific tests for the properties.
When depending on QuickCheck, we have to assume that it actually generates test cases that test such a property.
This is particularly difficult for moves that would normally never occur in a game of chess, since the Markov Chain would then not be trained on such moves.

Given that we train a Markov Chain to generate the tests, we can never ensure exhaustiveness of these tests.
Moves that never occur in any of the games supplied to the Markov Chain can never occur in a game that is subsequently generated by the Chain.

\subsection{Assumptions}
In some of the properties that we test we assume certain things about the library to be correct.
Ideally, one would never make such assumptions, but this choice was made in order to make the workload on ourselves slightly lighter.
The most significant of such assumptions is the \hs{check :: Color -> Board -> Bool} function.
Some of our properties make use of this function to test whether either of the players is in check.
In particular, \hs{prop_not_in_check_twice} and \hs{prop_move_not_result_check} use this function.

This assumption is not the only assumption made.
Another example is the \hs{pieceCoords} function, although that has been modified to suit our needs.

\section{QuickCheck Evaluation}
% Question 5.a
Installing QuickCheck on the testing system is easy.
With Haskell Platform\footnote{\url{http://haskell.org/platform}} available, \console{cabal update} and \console{cabal install QuickCheck} are sufficient to install the MBT tool.
However, implementing the \hs{Arbitrary} instance that QuickCheck requires for test generation may be hard.
This is the case for our SUT with the generation of \hs{PGN}, as mentioned before.
Furthermore, the quality of the generated test set has a large impact on the test coverage.
If the implementation of the \hs{Arbitrary} class is not complete, then neither is the test generation, nor the test itself.

% Question 5.b
Nonetheless, if the \hs{Arbitrary} instance is satisfactory, then it is easy enough to express many possible properties of the SUT when testing with QuickCheck.
Programming properties mostly only involves straightforward Haskell code,
	as they are just pure functions that return a boolean(-like) type.%
	\footnote{QuickCheck has options to test programs that use the IO monad, but we did not have to use that in our tests.}
In some cases, with complex data types, programming properties can be tedious.
However, when a proper formal model underlies the Haskell code, mathematical properties on the model should translate relatively easily to QuickCheck properties.
For our SUT, we did not have problems with this,
	thanks to some useful abstractions.
For instance, we defined helper functions like \hs{pieceCoords} and a group of higher-order property transformers like \hs{atAnyTwoStates},
	which takes a property on two consecutive boards and transforms it into a property of a \hs{PGN}.

% Question 5.c
Both manual and automated testing have advantages.
Manual testing allows for a great control over the test cases
	and thus enables the tester to test very specific properties.
However, creating the tests is rather laborious
	and usually this method will not find bugs that is not explicitly tested for.
Automated testing can find such bugs,
	because it works with more abstract and general properties.
While test case generation can be complicated,
	it allows you to test properties on a large amount of cases,
	thus improving coverage.
We have seen in this assignment and assignment 1 that both approaches yield valuable results
	and would therefore argue for a hybrid approach
	in which simple, abstract properties are tested with automated testing
	and domain experts are used to manually test edge cases.

% Question 5.d
Although we did not use it, QuickCheck is able to handle non-determinism.
Note that Haskell is a pure programming language, making concepts such as randomness tricky.
Typically, non-determinism in Haskell is implemented via the \hs{IO} monad, such as in \hs{System.Random}.%
	\footnote{\url{http://hackage.haskell.org/package/random}}
\sloppy % Test.QuickCheck.Monad causes an overfull \hbox
As a consequence, properties involving non-deterministic functions are defined in a monadic style using \hs{monadicIO}, \hs{assert} (available in \hs{Test.QuickCheck.Monadic}) and \hs{do} notation.%
	\footnote{\url{http://hackage.haskell.org/package/QuickCheck/docs/Test-QuickCheck-Monadic.html}}
For an example, consider the example test below.

\begin{minted}{haskell}
	import System.Random

	randomNumberInRange :: Integer -> Integer -> IO Integer
	randomNumberInRange lower upper = randomRIO (lower, upper)

	import Test.QuickCheck
	import Test.QuickCheck.Monadic

	prop_random_number_is_in_range :: Integer -> Integer -> Property
	prop_random_number_is_in_range lower upper = monadicIO $ do
		result <- randomNumberInRange lower upper
		assert $ lower <= result && result <= upper
\end{minted}
%
Given that Chess is deterministic given a sequence of moves and that the components of the \texttt{chesshs} package under test do not contain monadic programming, \hs{Test.QuickCheck.Monadic} was not used in our testing system.

% Question 5.e
QuickCheck allows for selection of test cases by implementing different instances of \hs{Arbitrary}.
Furthermore, \hs{forAll :: (Show a,Testable prop) => Gen a -> (a -> prop) -> Property}
	allows the tester to test with a custom generator rather than the one implemented in the \hs{Arbitrary} instance.
By default, QuickCheck will generate random test cases.
There is an option to fix the random seed so that the same test cases are used in each run.
We did not do this, because we might miss useful test cases like that.

% Question 5.f
Due to lazy evaluation, test cases are generated as they are needed.
However, QuickCheck does not generate test cases dependent on the results from previous tests.
Also, it reuses the same test set for every property that is tested.

% Question 5.g
There is no formal (implementation) relation between the model (the collection of all the properties) and the SUT.
The relation between the SUT and the model is Chess, more specifically, its rules.
The SUT uses the rules of Chess as a specification to its implementation.
The model transforms the rules of Chess into a number of properties.
If the SUT does not contain any faults, the properties should hold during its execution.
Simply put, the model is closer to the specification than to the implementation.
It is this connection which makes testing with QuickCheck intuitive (black box): only the specification and the interface of the SUT are required to define and implement the set of properties.

\begin{thebibliography}{8}
	\bibitem[PGN]  {PGN}  \url{http://saremba.de/chessgml/standards/pgn/pgn-complete.htm}
\end{thebibliography}

\end{document}
