model size 1
0: Tue Oct 24 20:50:08 CEST 2017
Hypothesis size: 1 states
learning queries/symbols: 5/5(5/5 this learning round)
testing queries/symbols: 22/34(17/29 this testing round)

model size 2
1: Tue Oct 24 20:50:12 CEST 2017
Hypothesis size: 2 states
learning queries/symbols: 41/73(19/39 this learning round)
testing queries/symbols: 52/101(11/28 this testing round)

model size 3
2: Tue Oct 24 20:50:20 CEST 2017
Hypothesis size: 3 states
learning queries/symbols: 81/188(29/87 this learning round)
testing queries/symbols: 177/558(96/370 this testing round)

model size 4
3: Tue Oct 24 20:50:53 CEST 2017
Hypothesis size: 4 states
learning queries/symbols: 217/733(40/175 this learning round)
testing queries/symbols: 344/1263(127/530 this testing round)

model size 5
4: Tue Oct 24 20:51:39 CEST 2017
Hypothesis size: 5 states
learning queries/symbols: 394/1475(50/212 this learning round)
testing queries/symbols: 532/2022(138/547 this testing round)

model size 7
5: Tue Oct 24 20:52:45 CEST 2017
Hypothesis size: 7 states
learning queries/symbols: 622/2551(90/529 this learning round)
testing queries/symbols: 636/2594(14/43 this testing round)

model size 8
6: Tue Oct 24 20:53:07 CEST 2017
Hypothesis size: 8 states
learning queries/symbols: 710/2939(74/345 this learning round)
testing queries/symbols: 1513/7096(803/4157 this testing round)

model size 9
7: Tue Oct 24 20:57:48 CEST 2017
Hypothesis size: 9 states
learning queries/symbols: 1598/7587(85/491 this learning round)
*************************
A coding exception was thrown and uncaught in a Task.

Full message: TypeError: NetworkError when attempting to fetch resource.
Full stack: 
*************************
testing queries/symbols: 2598/13051(1000/5464 this testing round)

model size 10
8: Tue Oct 24 21:04:39 CEST 2017
Hypothesis size: 10 states
learning queries/symbols: 2694/13676(96/625 this learning round)
testing queries/symbols: 3870/20326(1176/6650 this testing round)

model size 11
9: Tue Oct 24 21:14:05 CEST 2017
Hypothesis size: 11 states
learning queries/symbols: 3976/21079(106/753 this learning round)
testing queries/symbols: 10566/65428(6590/44349 this testing round)

Finished learning!
Done: Tue Oct 24 22:29:24 CEST 2017
