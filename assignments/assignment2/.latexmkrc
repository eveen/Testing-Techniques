# vim: syntax=perl:
$pdflatex = 'internal pdflatexwithdot %R %O %S';

sub pdflatexwithdot {
	for (my $i = 0; $i <= 6; $i++) {
		system("dot -Tpng LStar_hypothesis" . $i . ".dot > LStar_hypothesis" . $i . ".png");
	}
	return system("pdflatex -shell-escape @_");
}
