import basiclearner.BasicLearner;
import basiclearner.SocketSUL;
import brpcompare.BRPCompare;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableSet;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;

public class Main {

    public static void main(String[] arguments) throws Exception {
        test("Reference");

        for (int index = 1; index <= 6; index++)
            test(Integer.toString(index));

        System.out.println("###########");
    }

    private static void test(String name) throws Exception {
        display("########### Testing '" + name + "' ###########");

        display("Now it was " + LocalDateTime.now() + ".");
        Stopwatch stopWatch = Stopwatch.createStarted();

        learn(name);

        checkOutPutOfFinalModelOf(name);

        if (!name.equals("Reference"))
            checkFinalModelOf(name);

        display("===========");

        stopWatch.stop();
        display( "Now it was " + LocalDateTime.now() + ".\n"
               + "Testing '" + name  + "' took " + stopWatch + "."
               );
    }

    private static void learn(String name) throws Exception {
        display("=========== Learning '" + name + "' ===========");

        File directory = new File( new File(System.getProperty("user.dir"))
                                 , "Bounded Retransmission Protocol Implementations"
                                 );
        // Throws : IOException
        Process process = Runtime.getRuntime().exec("java -jar " + name + ".jar", null, directory);
        display( "Started \"" + name + ".jar\" in \"" + directory + "\".\n"
               + "Waiting a while for it to get ready."
               );
        // Throws : InterruptedException
        process.waitFor(1, TimeUnit.MINUTES);
        display("Assuming \"" + name + ".jar\" is ready.");

        InetAddress internetProtocolAddress = InetAddress.getLoopbackAddress();
        int port = parsePort(new BufferedReader(new InputStreamReader(process.getInputStream())));
        // Throws : UnknownHostException
        SocketSUL system = new SocketSUL( internetProtocolAddress
                                        , port
                                        , true
                                        , "reset"
                                        );
        display("Created socket system @ " + internetProtocolAddress + " : " + port + ".");

        display("~~~~~~~~~~~");

        // Default : BasicLearner.randomWalk_numberOfSymbols = 300;
        BasicLearner.randomWalk_numberOfSymbols = 1001;
        // Default : BasicLearner.randomWalk_chanceOfResetting = 0.1;
        BasicLearner.randomWalk_chanceOfResetting = 0.01;
        BasicLearner.INTERMEDIATE_HYPOTHESIS_FILENAME = "Hypothesis Model Of " + name + " ";
        BasicLearner.FINAL_MODEL_FILENAME = "Final Model Of " + name;
        Collection<String> input = ImmutableSet.of( "IACK"
                                                  , "IREQ_0_0_0"
                                                  , "IREQ_0_0_1"
                                                  , "IREQ_0_1_0"
                                                  , "IREQ_0_1_1"
                                                  , "IREQ_1_0_0"
                                                  , "IREQ_1_0_1"
                                                  , "IREQ_1_1_0"
                                                  , "IREQ_1_1_1"
                                                  , "ISENDFRAME"
                                                  , "ITIMEOUT"
                                                  );
        // Throws : IOException
        BasicLearner.runControlledExperiment( system
                                            , BasicLearner.LearningMethod.LStar
                                            , BasicLearner.TestingMethod.RandomWalk
                                            , input
                                            );

        System.out.println();
        display("~~~~~~~~~~~");

        // Throws : Exception
        system.close();
        display("Closed socket system.");

        process.destroy();
        display( "Stopped \"" + name + ".jar\" gracefully.\n"
               + "Waiting (a while) for it to stop gracefully."
               );
        // Throws : InterruptedException
        if (process.waitFor(1, TimeUnit.MINUTES))
            display("\"" + name + ".jar\" stopped gracefully.");
        else {
            display( "Assuming \"" + name + ".jar\" is not responding.\n"
                   + "Stopping it forcibly."
                   );
            process.destroyForcibly();
            display( "Stopped \"" + name + ".jar\" forcibly.\n"
                   + "Waiting (a while) for it to stop forcibly."
                   );
            // Throws : InterruptedException
            if (process.waitFor(1, TimeUnit.MINUTES))
                display("\"" + name + ".jar\" stopped forcibly.");
            else
                display("Assuming \"" + name + ".jar\" stopped forcibly.");
        }
    }

    private static int parsePort(BufferedReader outPut) throws Exception {
        String line = "";
        for (int index = 1; index <= 4; index ++)
            // Throws : IOException
            line = outPut.readLine();

        return new Scanner(line).useDelimiter("[^0-9]+").nextInt();
    }

    private static void checkOutPutOfFinalModelOf(String name) throws Exception {
        display("=========== Checking The Out Put Of The Final Model Of '" + name + "' ===========");

        // Throws : FileNotFoundException
        Scanner scanner = new Scanner(new File( new File(System.getProperty("user.dir"))
                                              , "Final Model Of " + name + ".dot"
                                              ));
        boolean hasOCONF_0 = false, hasOCONF_1 = false, hasOCONF_2 = false;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!hasOCONF_0 && line.contains("OCONF_0"))
                hasOCONF_0 = true;
            if (!hasOCONF_1 && line.contains("OCONF_1"))
                hasOCONF_1 = true;
            if (!hasOCONF_2 && line.contains("OCONF_2"))
                hasOCONF_2 = true;
            if (hasOCONF_0 && hasOCONF_1 && hasOCONF_2) {
                display( "The final model of '"
                       + name
                       + "' contains OCONF_0, OCONF_1 & OCONF_2 as out puts."
                       );
                return;
            }
        }
        System.out.println("The final model of '" + name + "' does not contain ___ as out put.");
        if (!hasOCONF_0)
            System.out.println("OCONF_0");
        if (!hasOCONF_1)
            System.out.println("OCONF_1");
        if (!hasOCONF_2)
            System.out.println("OCONF_2");
        System.out.println();
    }

    private static void checkFinalModelOf(String name) throws Exception {
        display("=========== Checking The Final Model Of '" + name + "' ===========");

        display("~~~~~~~~~~~");

        String[] arguments = {name};
        // Throws : IOException
        BRPCompare.main(arguments);

        System.out.println();
        display("~~~~~~~~~~~");
    }

    private static void display(String string) {
        System.out.println(string);
        System.out.println();
    }
}
