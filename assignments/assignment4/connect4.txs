TYPEDEF Colour ::= Empty | Red | Yellow ENDDEF
TYPEDEF Column ::= Column
	{ a :: Colour
	; b :: Colour
	; c :: Colour
	; d :: Colour
	; e :: Colour
	; f :: Colour
	} ENDDEF
TYPEDEF State ::= State
	{ c0 :: Column
	; c1 :: Column
	; c2 :: Column
	; c3 :: Column
	; c4 :: Column
	; c5 :: Column
	; c6 :: Column
	} ENDDEF

TYPEDEF MoveResult ::= Ok | RedWin | YellowWin | Tie | Error ENDDEF

CHANDEF Channels ::= Col :: Int; MoveRes :: MoveResult ENDDEF

TYPEDEF ColResult ::= ColResult { col :: Column; res :: MoveResult } ENDDEF

FUNCDEF playInColumn(col :: Column; colour :: Colour) :: ColResult ::=
	     IF isEmpty(a(col)) THEN ColResult (Column (colour, b(col), c(col), d(col), e(col), f(col)), Ok)
	ELSE IF isEmpty(b(col)) THEN ColResult (Column (a(col), colour, c(col), d(col), e(col), f(col)), Ok)
	ELSE IF isEmpty(c(col)) THEN ColResult (Column (a(col), b(col), colour, d(col), e(col), f(col)), Ok)
	ELSE IF isEmpty(d(col)) THEN ColResult (Column (a(col), b(col), c(col), colour, e(col), f(col)), Ok)
	ELSE IF isEmpty(e(col)) THEN ColResult (Column (a(col), b(col), c(col), d(col), colour, f(col)), Ok)
	ELSE IF isEmpty(f(col)) THEN ColResult (Column (a(col), b(col), c(col), d(col), e(col), colour), Ok)
	ELSE ColResult (col, Error)
	FI FI FI FI FI FI
ENDDEF

TYPEDEF StateResult ::= StateResult { st :: State; res :: MoveResult } ENDDEF

FUNCDEF colourToWin(col :: Colour) :: MoveResult ::=
	IF isRed(col) THEN RedWin
	ELSE IF isYellow(col) THEN YellowWin
	ELSE Ok
	FI FI
ENDDEF

FUNCDEF colResult(col :: Column) :: MoveResult ::=
	IF isEmpty(c(col)) THEN
		Ok
	ELSE IF (a(col) == b(col)) /\ (b(col) == c(col)) /\ (c(col) == d(col)) THEN colourToWin(a(col))
	ELSE IF (b(col) == c(col)) /\ (c(col) == d(col)) /\ (d(col) == e(col)) THEN colourToWin(b(col))
	ELSE IF (c(col) == d(col)) /\ (d(col) == e(col)) /\ (e(col) == f(col)) THEN colourToWin(c(col))
	ELSE Ok
	FI FI FI FI
ENDDEF

FUNCDEF verticalResult(st :: State) :: MoveResult ::=
	IF      isFinal(colResult(c0(st))) THEN colResult(c0(st))
	ELSE IF isFinal(colResult(c1(st))) THEN colResult(c1(st))
	ELSE IF isFinal(colResult(c2(st))) THEN colResult(c2(st))
	ELSE IF isFinal(colResult(c3(st))) THEN colResult(c3(st))
	ELSE IF isFinal(colResult(c4(st))) THEN colResult(c4(st))
	ELSE IF isFinal(colResult(c5(st))) THEN colResult(c5(st))
	ELSE IF isFinal(colResult(c6(st))) THEN colResult(c6(st))
	ELSE Ok
	FI FI FI FI FI FI FI
ENDDEF

FUNCDEF horizontalResult(st :: State) :: MoveResult ::=
	-- Row A
	IF      not(isEmpty(a(c3(st)))) /\ (a(c0(st)) == a(c1(st))) /\ (a(c1(st)) == a(c2(st))) /\ (a(c2(st)) == a(c3(st))) THEN colourToWin(a(c3(st)))
	ELSE IF not(isEmpty(a(c3(st)))) /\ (a(c1(st)) == a(c2(st))) /\ (a(c2(st)) == a(c3(st))) /\ (a(c3(st)) == a(c4(st))) THEN colourToWin(a(c3(st)))
	ELSE IF not(isEmpty(a(c3(st)))) /\ (a(c2(st)) == a(c3(st))) /\ (a(c3(st)) == a(c4(st))) /\ (a(c4(st)) == a(c5(st))) THEN colourToWin(a(c3(st)))
	ELSE IF not(isEmpty(a(c3(st)))) /\ (a(c3(st)) == a(c4(st))) /\ (a(c4(st)) == a(c5(st))) /\ (a(c5(st)) == a(c6(st))) THEN colourToWin(a(c3(st)))
	-- Row B
	ELSE IF not(isEmpty(b(c3(st)))) /\ (b(c0(st)) == b(c1(st))) /\ (b(c1(st)) == b(c2(st))) /\ (b(c2(st)) == b(c3(st))) THEN colourToWin(b(c3(st)))
	ELSE IF not(isEmpty(b(c3(st)))) /\ (b(c1(st)) == b(c2(st))) /\ (b(c2(st)) == b(c3(st))) /\ (b(c3(st)) == b(c4(st))) THEN colourToWin(b(c3(st)))
	ELSE IF not(isEmpty(b(c3(st)))) /\ (b(c2(st)) == b(c3(st))) /\ (b(c3(st)) == b(c4(st))) /\ (b(c4(st)) == b(c5(st))) THEN colourToWin(b(c3(st)))
	ELSE IF not(isEmpty(b(c3(st)))) /\ (b(c3(st)) == b(c4(st))) /\ (b(c4(st)) == b(c5(st))) /\ (b(c5(st)) == b(c6(st))) THEN colourToWin(b(c3(st)))
	-- Row C
	ELSE IF not(isEmpty(c(c3(st)))) /\ (c(c0(st)) == c(c1(st))) /\ (c(c1(st)) == c(c2(st))) /\ (c(c2(st)) == c(c3(st))) THEN colourToWin(c(c3(st)))
	ELSE IF not(isEmpty(c(c3(st)))) /\ (c(c1(st)) == c(c2(st))) /\ (c(c2(st)) == c(c3(st))) /\ (c(c3(st)) == c(c4(st))) THEN colourToWin(c(c3(st)))
	ELSE IF not(isEmpty(c(c3(st)))) /\ (c(c2(st)) == c(c3(st))) /\ (c(c3(st)) == c(c4(st))) /\ (c(c4(st)) == c(c5(st))) THEN colourToWin(c(c3(st)))
	ELSE IF not(isEmpty(c(c3(st)))) /\ (c(c3(st)) == c(c4(st))) /\ (c(c4(st)) == c(c5(st))) /\ (c(c5(st)) == c(c6(st))) THEN colourToWin(c(c3(st)))
	-- Row D
	ELSE IF not(isEmpty(d(c3(st)))) /\ (d(c0(st)) == d(c1(st))) /\ (d(c1(st)) == d(c2(st))) /\ (d(c2(st)) == d(c3(st))) THEN colourToWin(d(c3(st)))
	ELSE IF not(isEmpty(d(c3(st)))) /\ (d(c1(st)) == d(c2(st))) /\ (d(c2(st)) == d(c3(st))) /\ (d(c3(st)) == d(c4(st))) THEN colourToWin(d(c3(st)))
	ELSE IF not(isEmpty(d(c3(st)))) /\ (d(c2(st)) == d(c3(st))) /\ (d(c3(st)) == d(c4(st))) /\ (d(c4(st)) == d(c5(st))) THEN colourToWin(d(c3(st)))
	ELSE IF not(isEmpty(d(c3(st)))) /\ (d(c3(st)) == d(c4(st))) /\ (d(c4(st)) == d(c5(st))) /\ (d(c5(st)) == d(c6(st))) THEN colourToWin(d(c3(st)))
	-- Row E
	ELSE IF not(isEmpty(e(c3(st)))) /\ (e(c0(st)) == e(c1(st))) /\ (e(c1(st)) == e(c2(st))) /\ (e(c2(st)) == e(c3(st))) THEN colourToWin(e(c3(st)))
	ELSE IF not(isEmpty(e(c3(st)))) /\ (e(c1(st)) == e(c2(st))) /\ (e(c2(st)) == e(c3(st))) /\ (e(c3(st)) == e(c4(st))) THEN colourToWin(e(c3(st)))
	ELSE IF not(isEmpty(e(c3(st)))) /\ (e(c2(st)) == e(c3(st))) /\ (e(c3(st)) == e(c4(st))) /\ (e(c4(st)) == e(c5(st))) THEN colourToWin(e(c3(st)))
	ELSE IF not(isEmpty(e(c3(st)))) /\ (e(c3(st)) == e(c4(st))) /\ (e(c4(st)) == e(c5(st))) /\ (e(c5(st)) == e(c6(st))) THEN colourToWin(e(c3(st)))
	-- Row F
	ELSE IF not(isEmpty(f(c3(st)))) /\ (f(c0(st)) == f(c1(st))) /\ (f(c1(st)) == f(c2(st))) /\ (f(c2(st)) == f(c3(st))) THEN colourToWin(f(c3(st)))
	ELSE IF not(isEmpty(f(c3(st)))) /\ (f(c1(st)) == f(c2(st))) /\ (f(c2(st)) == f(c3(st))) /\ (f(c3(st)) == f(c4(st))) THEN colourToWin(f(c3(st)))
	ELSE IF not(isEmpty(f(c3(st)))) /\ (f(c2(st)) == f(c3(st))) /\ (f(c3(st)) == f(c4(st))) /\ (f(c4(st)) == f(c5(st))) THEN colourToWin(f(c3(st)))
	ELSE IF not(isEmpty(f(c3(st)))) /\ (f(c3(st)) == f(c4(st))) /\ (f(c4(st)) == f(c5(st))) /\ (f(c5(st)) == f(c6(st))) THEN colourToWin(f(c3(st)))
	ELSE
		Ok
	FI FI FI FI
	FI FI FI FI
	FI FI FI FI
	FI FI FI FI
	FI FI FI FI
	FI FI FI FI
ENDDEF

FUNCDEF diagonalResult(st :: State) :: MoveResult ::=
	-- Left bottom to right top
	IF      not(isEmpty(a(c0(st)))) /\ (a(c0(st)) == b(c1(st))) /\ (b(c1(st)) == c(c2(st))) /\ (c(c2(st)) == d(c3(st))) THEN colourToWin(c(c0(st)))
	ELSE IF not(isEmpty(a(c1(st)))) /\ (a(c1(st)) == b(c2(st))) /\ (b(c2(st)) == c(c3(st))) /\ (c(c3(st)) == d(c4(st))) THEN colourToWin(c(c1(st)))
	ELSE IF not(isEmpty(a(c2(st)))) /\ (a(c2(st)) == b(c3(st))) /\ (b(c3(st)) == c(c4(st))) /\ (c(c4(st)) == d(c5(st))) THEN colourToWin(c(c2(st)))
	ELSE IF not(isEmpty(a(c3(st)))) /\ (a(c3(st)) == b(c4(st))) /\ (b(c4(st)) == c(c5(st))) /\ (c(c5(st)) == d(c6(st))) THEN colourToWin(c(c3(st)))
	ELSE IF not(isEmpty(b(c0(st)))) /\ (b(c0(st)) == c(c1(st))) /\ (c(c1(st)) == d(c2(st))) /\ (d(c2(st)) == e(c3(st))) THEN colourToWin(c(c0(st)))
	ELSE IF not(isEmpty(b(c1(st)))) /\ (b(c1(st)) == c(c2(st))) /\ (c(c2(st)) == d(c3(st))) /\ (d(c3(st)) == e(c4(st))) THEN colourToWin(c(c1(st)))
	ELSE IF not(isEmpty(b(c2(st)))) /\ (b(c2(st)) == c(c3(st))) /\ (c(c3(st)) == d(c4(st))) /\ (d(c4(st)) == e(c5(st))) THEN colourToWin(c(c2(st)))
	ELSE IF not(isEmpty(b(c3(st)))) /\ (b(c3(st)) == c(c4(st))) /\ (c(c4(st)) == d(c5(st))) /\ (d(c5(st)) == e(c6(st))) THEN colourToWin(c(c3(st)))
	ELSE IF not(isEmpty(c(c0(st)))) /\ (c(c0(st)) == d(c1(st))) /\ (d(c1(st)) == e(c2(st))) /\ (e(c2(st)) == f(c3(st))) THEN colourToWin(c(c0(st)))
	ELSE IF not(isEmpty(c(c1(st)))) /\ (c(c1(st)) == d(c2(st))) /\ (d(c2(st)) == e(c3(st))) /\ (e(c3(st)) == f(c4(st))) THEN colourToWin(c(c1(st)))
	ELSE IF not(isEmpty(c(c2(st)))) /\ (c(c2(st)) == d(c3(st))) /\ (d(c3(st)) == e(c4(st))) /\ (e(c4(st)) == f(c5(st))) THEN colourToWin(c(c2(st)))
	ELSE IF not(isEmpty(c(c3(st)))) /\ (c(c3(st)) == d(c4(st))) /\ (d(c4(st)) == e(c5(st))) /\ (e(c5(st)) == f(c6(st))) THEN colourToWin(c(c3(st)))
	-- Left top to right bottom
	ELSE IF not(isEmpty(d(c0(st)))) /\ (d(c0(st)) == c(c1(st))) /\ (c(c1(st)) == b(c2(st))) /\ (b(c2(st)) == a(c3(st))) THEN colourToWin(c(c0(st)))
	ELSE IF not(isEmpty(d(c1(st)))) /\ (d(c1(st)) == c(c2(st))) /\ (c(c2(st)) == b(c3(st))) /\ (b(c3(st)) == a(c4(st))) THEN colourToWin(c(c1(st)))
	ELSE IF not(isEmpty(d(c2(st)))) /\ (d(c2(st)) == c(c3(st))) /\ (c(c3(st)) == b(c4(st))) /\ (b(c4(st)) == a(c5(st))) THEN colourToWin(c(c2(st)))
	ELSE IF not(isEmpty(d(c3(st)))) /\ (d(c3(st)) == c(c4(st))) /\ (c(c4(st)) == b(c5(st))) /\ (b(c5(st)) == a(c6(st))) THEN colourToWin(c(c3(st)))
	ELSE IF not(isEmpty(e(c0(st)))) /\ (e(c0(st)) == d(c1(st))) /\ (d(c1(st)) == c(c2(st))) /\ (c(c2(st)) == b(c3(st))) THEN colourToWin(c(c0(st)))
	ELSE IF not(isEmpty(e(c1(st)))) /\ (e(c1(st)) == d(c2(st))) /\ (d(c2(st)) == c(c3(st))) /\ (c(c3(st)) == b(c4(st))) THEN colourToWin(c(c1(st)))
	ELSE IF not(isEmpty(e(c2(st)))) /\ (e(c2(st)) == d(c3(st))) /\ (d(c3(st)) == c(c4(st))) /\ (c(c4(st)) == b(c5(st))) THEN colourToWin(c(c2(st)))
	ELSE IF not(isEmpty(e(c3(st)))) /\ (e(c3(st)) == d(c4(st))) /\ (d(c4(st)) == c(c5(st))) /\ (c(c5(st)) == b(c6(st))) THEN colourToWin(c(c3(st)))
	ELSE IF not(isEmpty(f(c0(st)))) /\ (f(c0(st)) == e(c1(st))) /\ (e(c1(st)) == d(c2(st))) /\ (d(c2(st)) == c(c3(st))) THEN colourToWin(c(c0(st)))
	ELSE IF not(isEmpty(f(c1(st)))) /\ (f(c1(st)) == e(c2(st))) /\ (e(c2(st)) == d(c3(st))) /\ (d(c3(st)) == c(c4(st))) THEN colourToWin(c(c1(st)))
	ELSE IF not(isEmpty(f(c2(st)))) /\ (f(c2(st)) == e(c3(st))) /\ (e(c3(st)) == d(c4(st))) /\ (d(c4(st)) == c(c5(st))) THEN colourToWin(c(c2(st)))
	ELSE IF not(isEmpty(f(c3(st)))) /\ (f(c3(st)) == e(c4(st))) /\ (e(c4(st)) == d(c5(st))) /\ (d(c5(st)) == c(c6(st))) THEN colourToWin(c(c3(st)))
	ELSE Ok
	FI FI FI FI FI FI FI FI FI FI FI FI
	FI FI FI FI FI FI FI FI FI FI FI FI
ENDDEF

FUNCDEF isFinal(res :: MoveResult) :: Bool ::=
	isRedWin(res) \/ isYellowWin(res) \/ isTie(res)
ENDDEF

FUNCDEF check(st :: State; res :: MoveResult) :: StateResult ::=
	IF      isError(res)                  THEN StateResult(st,res)
	ELSE IF isFinal(verticalResult(st))   THEN StateResult(st, verticalResult(st))
	ELSE IF isFinal(horizontalResult(st)) THEN StateResult(st, horizontalResult(st))
	ELSE IF isFinal(diagonalResult(st))   THEN StateResult(st, diagonalResult(st))
	ELSE IF
			not(isEmpty(f(c0(st)))) /\
			not(isEmpty(f(c1(st)))) /\
			not(isEmpty(f(c2(st)))) /\
			not(isEmpty(f(c3(st)))) /\
			not(isEmpty(f(c4(st)))) /\
			not(isEmpty(f(c5(st)))) /\
			not(isEmpty(f(c6(st)))) THEN
		StateResult(st, Tie)
	ELSE
		StateResult(st, Ok)
	FI FI FI FI FI
ENDDEF

FUNCDEF play(st :: State; col :: Int; colour :: Colour) :: StateResult ::=
	IF col == 0 THEN
		LET cres = playInColumn(c0(st), colour)
		IN  check(State (col(cres), c1(st), c2(st), c3(st), c4(st), c5(st), c6(st)), res(cres)) NI
	ELSE IF col == 1 THEN
		LET cres = playInColumn(c1(st), colour)
		IN  check(State (c0(st), col(cres), c2(st), c3(st), c4(st), c5(st), c6(st)), res(cres)) NI
	ELSE IF col == 2 THEN
		LET cres = playInColumn(c2(st), colour)
		IN  check(State (c0(st), c1(st), col(cres), c3(st), c4(st), c5(st), c6(st)), res(cres)) NI
	ELSE IF col == 3 THEN
		LET cres = playInColumn(c3(st), colour)
		IN  check(State (c0(st), c1(st), c2(st), col(cres), c4(st), c5(st), c6(st)), res(cres)) NI
	ELSE IF col == 4 THEN
		LET cres = playInColumn(c4(st), colour)
		IN  check(State (c0(st), c1(st), c2(st), c3(st), col(cres), c5(st), c6(st)), res(cres)) NI
	ELSE IF col == 5 THEN
		LET cres = playInColumn(c5(st), colour)
		IN  check(State (c0(st), c1(st), c2(st), c3(st), c4(st), col(cres), c6(st)), res(cres)) NI
	ELSE IF col == 6 THEN
		LET cres = playInColumn(c6(st), colour)
		IN  check(State (c0(st), c1(st), c2(st), c3(st), c4(st), c5(st), col(cres)), res(cres)) NI
	ELSE
		StateResult(st, Error)
	FI FI FI FI FI FI FI
ENDDEF

FUNCDEF initStateResult () :: StateResult ::=
	StateResult
		(State
			( Column (Empty,Empty,Empty,Empty,Empty,Empty)
			, Column (Empty,Empty,Empty,Empty,Empty,Empty)
			, Column (Empty,Empty,Empty,Empty,Empty,Empty)
			, Column (Empty,Empty,Empty,Empty,Empty,Empty)
			, Column (Empty,Empty,Empty,Empty,Empty,Empty)
			, Column (Empty,Empty,Empty,Empty,Empty,Empty)
			, Column (Empty,Empty,Empty,Empty,Empty,Empty))
		, Ok
		)
ENDDEF

STAUTDEF model [Col :: Int; MoveRes :: MoveResult] () ::=
	VAR stres :: StateResult
	STATE sred, sred2, syellow, syellow2, sdone
	INIT sred { stres := initStateResult() }
	TRANS
		sred -> MoveRes ! YellowWin [[ isYellowWin(res(stres)) ]] {stres := initStateResult()} -> sred
		sred -> MoveRes ! Tie       [[ isTie(res(stres)) ]]       {stres := initStateResult()} -> sred

		sred -> Col ? c [[ c == 0 ]] {stres := play(st(stres), 0, Red)} -> sred2
		sred -> Col ? c [[ c == 1 ]] {stres := play(st(stres), 1, Red)} -> sred2
		sred -> Col ? c [[ c == 2 ]] {stres := play(st(stres), 2, Red)} -> sred2
		sred -> Col ? c [[ c == 3 ]] {stres := play(st(stres), 3, Red)} -> sred2
		sred -> Col ? c [[ c == 4 ]] {stres := play(st(stres), 4, Red)} -> sred2
		sred -> Col ? c [[ c == 5 ]] {stres := play(st(stres), 5, Red)} -> sred2
		sred -> Col ? c [[ c == 6 ]] {stres := play(st(stres), 6, Red)} -> sred2

		sred2 -> MoveRes ! Ok    [[ not(isError(res(stres))) ]] -> syellow
		sred2 -> MoveRes ! Error [[ isError(res(stres)) ]] -> sred

		syellow -> MoveRes ! RedWin [[ isRedWin(res(stres)) ]] {stres := initStateResult()} -> sred
		syellow -> MoveRes ! Tie    [[ isTie(res(stres)) ]]    {stres := initStateResult()} -> sred

		syellow -> Col ? c [[ c == 0 ]] {stres := play(st(stres), 0, Yellow)} -> syellow2
		syellow -> Col ? c [[ c == 1 ]] {stres := play(st(stres), 1, Yellow)} -> syellow2
		syellow -> Col ? c [[ c == 2 ]] {stres := play(st(stres), 2, Yellow)} -> syellow2
		syellow -> Col ? c [[ c == 3 ]] {stres := play(st(stres), 3, Yellow)} -> syellow2
		syellow -> Col ? c [[ c == 4 ]] {stres := play(st(stres), 4, Yellow)} -> syellow2
		syellow -> Col ? c [[ c == 5 ]] {stres := play(st(stres), 5, Yellow)} -> syellow2
		syellow -> Col ? c [[ c == 6 ]] {stres := play(st(stres), 6, Yellow)} -> syellow2

		syellow2 -> MoveRes ! Ok    [[ not(isError(res(stres))) ]] -> sred
		syellow2 -> MoveRes ! Error [[ isError(res(stres)) ]] -> syellow
ENDDEF

MODELDEF Model ::=
	CHAN IN  Col
	CHAN OUT MoveRes
	BEHAVIOUR model[Col, MoveRes]()
ENDDEF

CNECTDEF Sut ::=
	CLIENTSOCK
	CHAN OUT Col HOST "localhost" PORT 1234
	ENCODE Col ? c -> ! toString(c)
	CHAN IN MoveRes HOST "localhost" PORT 1234
	DECODE MoveRes ! fromString(mr) <- ? mr
ENDDEF
