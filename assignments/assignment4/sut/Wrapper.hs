-- vim: sw=2 ts=2 et:
module Wrapper where

import qualified Board as ConnectFour

-- Simple Socket Library : Package 'network-simple' (haskell.hackage.org/package/network-simple)
import Network.Simple.TCP hiding (send)
import qualified Network.Simple.TCP (send)

import Data.ByteString.Char8 (pack, unpack)
import Data.Maybe
import Control.Monad

main :: IO ()
main = do
  putStrLn $ "Listening on port " ++ port
  serve localHost port $ \(socket, remoteEndAddress) -> forever $ do
    display "###########"
    display $ "Handler started for client @ '" ++ show remoteEndAddress ++ "'."
    display $ showBoard startBoard
    endBoard <- wrapper socket startBoard
    display $ showBoard endBoard
    display $ "Handler stopped for client @ '" ++ show remoteEndAddress ++ "'."
  where
    localHost = Host "127.0.0.1"
    port = "1234"

showBoard :: ConnectFour.Board -> String
showBoard = (++ "\n") . ConnectFour.showBoard

startBoard :: ConnectFour.Board
startBoard = ConnectFour.newBoard (6, width)

width :: Int
width = 7

wrapper :: Socket -> ConnectFour.Board -> IO ConnectFour.Board
wrapper socket board = case isFinishedGame board of
  (True, result) -> do
    display "==========="
    display $ "Game finished with result '" ++ result ++ "'."
    send socket result
    display "==========="
    return board
  (False, _) -> do
    display "==========="
    input <- receive socket
    if input == stop then do
      display "Game stopped."
      send socket "STOPPED"
      display "==========="
      return board
    else if input == "" then do
      wrapper socket board
    else case parseColumn input of
      Nothing -> do
        display $ "Error : '" ++ input ++ "' is not a column!"
        send socket "Error"
        wrapper socket board
      Just column -> if ConnectFour.isLegalMove board column then do
          nextBoard <- return $ ConnectFour.makeMove board column (ConnectFour.turn board)
          display $ showBoard nextBoard
          send socket "Ok"
          wrapper socket nextBoard
        else do
          display $ "Error : The move " ++ show column ++ " is not legal!"
          send socket "Error"
          wrapper socket board

isFinishedGame :: ConnectFour.Board -> (Bool, String)
isFinishedGame board | ConnectFour.winner board == 1    = (True , "RedWin"   )
                     | ConnectFour.winner board == 2    = (True , "YellowWin")
                     | ConnectFour.isDraw board == True = (True , "Tie"      )
                     | otherwise                        = (False, ""         )

parseColumn :: String -> Maybe ConnectFour.Column
parseColumn string = case reads string :: [(ConnectFour.Column, String)] of
  [(column, "")] -> Just column
  _              -> Nothing

-- ######## Utilities ###############################################################################

-- ======== Console =================================================================================

display :: String -> IO ()
display = putStrLn

-- ======== Socket ==================================================================================

stop :: String
stop = "STOP"

send :: Socket -> String -> IO ()
send socket string = do
  Network.Simple.TCP.send socket $ pack $ string ++ "\n"
  display $ "Sent '" ++ string ++ "'."

receive :: Socket -> IO String
receive socket = do
  -- The size of the in put (bytes) is multiplied by 11 to support every character encoding with at most 11 bytes per character.
  maybeByteString <- recv socket $ 11 * (length . show) width
  if isJust maybeByteString then do
    string <- return $ (unpack . fromJust) maybeByteString
    display $ "Received '" ++ string ++ "'."
    return $ filter (/= '\n') string
  else do
    display $ "Error : Wrapper.receive : Network.Simple.TCP.recv ~> Nothing!\n"
           ++ "\tInterpreting receiving '" ++ stop ++ "'."
    return stop
